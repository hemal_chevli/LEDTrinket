EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:8x8_led_matrix
LIBS:RVMH-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes -1300 2150 0    60   ~ 0
TODO\nLiPo charger ckt
$Comp
L ATTINY2313-P IC1
U 1 1 56306CB3
P 4150 2250
F 0 "IC1" H 3200 3250 40  0000 C CNN
F 1 "ATTINY2313-P" H 4950 1350 40  0000 C CNN
F 2 "SOIC_Packages:SOIC-20_7.5x12.8mm_Pitch1.27mm" H 4150 2250 35  0000 C CIN
F 3 "" H 4150 2250 60  0000 C CNN
	1    4150 2250
	1    0    0    -1  
$EndComp
$Comp
L 8x8_LED_Matrix U1
U 1 1 56308C97
P 1600 2950
F 0 "U1" H 1600 2450 60  0000 C CNN
F 1 "8x8_LED_Matrix" H 1600 3400 60  0000 C CNN
F 2 "terminal:8X8LEDMATRIX" H 1600 2950 60  0001 C CNN
F 3 "" H 1600 2950 60  0000 C CNN
	1    1600 2950
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X02 P1
U 1 1 56308D2E
P 1150 850
F 0 "P1" H 1150 1000 50  0000 C CNN
F 1 "CR2032" V 1250 850 50  0000 C CNN
F 2 "terminal:CR2032_noOUTline" H 1150 850 60  0001 C CNN
F 3 "" H 1150 850 60  0000 C CNN
	1    1150 850 
	-1   0    0    -1  
$EndComp
Text Label 750  2600 0    60   ~ 0
D0
Text Label 750  2700 0    60   ~ 0
D1
Text Label 750  2800 0    60   ~ 0
A1
Text Label 750  2900 0    60   ~ 0
A0
Text Label 750  3000 0    60   ~ 0
D2
Text Label 750  3100 0    60   ~ 0
D3
Text Label 750  3200 0    60   ~ 0
D4
Text Label 750  3300 0    60   ~ 0
D5
Text Label 2500 3300 0    60   ~ 0
B0
Text Label 2500 3200 0    60   ~ 0
B1
Text Label 2500 3100 0    60   ~ 0
B2
Text Label 2500 3000 0    60   ~ 0
B3
Text Label 2500 2900 0    60   ~ 0
B4
Text Label 2500 2800 0    60   ~ 0
B5
Text Label 2500 2700 0    60   ~ 0
B6
Text Label 2500 2600 0    60   ~ 0
B7
Wire Wire Line
	1050 2600 750  2600
Wire Wire Line
	750  2700 1050 2700
Wire Wire Line
	750  2800 1050 2800
Wire Wire Line
	750  2900 1050 2900
Wire Wire Line
	750  3000 1050 3000
Wire Wire Line
	750  3100 1050 3100
Wire Wire Line
	750  3200 1050 3200
Wire Wire Line
	750  3300 1050 3300
Wire Wire Line
	2500 3300 2150 3300
Wire Wire Line
	2500 3200 2150 3200
Wire Wire Line
	2500 3100 2150 3100
Wire Wire Line
	2500 3000 2150 3000
Wire Wire Line
	2500 2900 2150 2900
Wire Wire Line
	2500 2800 2150 2800
Wire Wire Line
	2500 2700 2150 2700
Wire Wire Line
	2500 2600 2150 2600
$Comp
L VCC #PWR01
U 1 1 56309A5E
P 2700 900
F 0 "#PWR01" H 2700 750 50  0001 C CNN
F 1 "VCC" H 2700 1050 50  0000 C CNN
F 2 "" H 2700 900 60  0000 C CNN
F 3 "" H 2700 900 60  0000 C CNN
	1    2700 900 
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR02
U 1 1 56309A78
P 1450 1050
F 0 "#PWR02" H 1450 800 50  0001 C CNN
F 1 "GND" H 1450 900 50  0000 C CNN
F 2 "" H 1450 1050 60  0000 C CNN
F 3 "" H 1450 1050 60  0000 C CNN
	1    1450 1050
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 1050 4150 1150
$Comp
L VCC #PWR03
U 1 1 56309BA3
P 4150 1050
F 0 "#PWR03" H 4150 900 50  0001 C CNN
F 1 "VCC" H 4150 1200 50  0000 C CNN
F 2 "" H 4150 1050 60  0000 C CNN
F 3 "" H 4150 1050 60  0000 C CNN
	1    4150 1050
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 56309C38
P 4150 3350
F 0 "#PWR04" H 4150 3100 50  0001 C CNN
F 1 "GND" H 4150 3200 50  0000 C CNN
F 2 "" H 4150 3350 60  0000 C CNN
F 3 "" H 4150 3350 60  0000 C CNN
	1    4150 3350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4150 3350 4150 3250
$Comp
L CONN_02X03 P2
U 1 1 56309C82
P 1450 1800
F 0 "P2" H 1450 2000 50  0000 C CNN
F 1 "ISP" H 1450 1600 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x03" H 1450 600 60  0001 C CNN
F 3 "" H 1450 600 60  0000 C CNN
	1    1450 1800
	1    0    0    -1  
$EndComp
Wire Wire Line
	5300 1450 5500 1450
Wire Wire Line
	5300 1550 5500 1550
Wire Wire Line
	5300 1650 5500 1650
Wire Wire Line
	5300 1750 5500 1750
Wire Wire Line
	5300 1850 5500 1850
Wire Wire Line
	5500 1950 5300 1950
Wire Wire Line
	5500 2050 5300 2050
Wire Wire Line
	5500 2150 5300 2150
Wire Wire Line
	5500 2350 5300 2350
Wire Wire Line
	5500 2450 5300 2450
Wire Wire Line
	5300 2550 5500 2550
Wire Wire Line
	5300 2650 5500 2650
Wire Wire Line
	5300 2750 5500 2750
Wire Wire Line
	5300 2850 5500 2850
Wire Wire Line
	3000 1950 2750 1950
Wire Wire Line
	2750 1750 3000 1750
Text Label 2750 1750 0    60   ~ 0
A1
Text Label 2750 1950 0    60   ~ 0
A0
Text Label 5500 1450 0    60   ~ 0
B0
Text Label 5500 1550 0    60   ~ 0
B1
Text Label 5500 1650 0    60   ~ 0
B2
Text Label 5500 1750 0    60   ~ 0
B3
Text Label 5500 1850 0    60   ~ 0
B4
Text Label 5500 1950 0    60   ~ 0
B5
Text Label 5500 2050 0    60   ~ 0
B6
Text Label 5500 2150 0    60   ~ 0
B7
Text Label 5500 2350 0    60   ~ 0
D0
Text Label 5500 2450 0    60   ~ 0
D1
Text Label 5500 2550 0    60   ~ 0
D2
Text Label 5500 2650 0    60   ~ 0
D3
Text Label 5500 2750 0    60   ~ 0
D4
Text Label 5500 2850 0    60   ~ 0
D5
$Comp
L R R1
U 1 1 5630B10C
P 2700 1450
F 0 "R1" V 2780 1450 50  0000 C CNN
F 1 "10K" V 2700 1450 50  0000 C CNN
F 2 "Resistors_SMD:R_0805" V 2630 1450 30  0001 C CNN
F 3 "" H 2700 1450 30  0000 C CNN
	1    2700 1450
	0    1    1    0   
$EndComp
Wire Wire Line
	2850 1450 3000 1450
Wire Wire Line
	2550 1100 4150 1100
Wire Wire Line
	2550 1100 2550 1450
Connection ~ 4150 1100
Wire Wire Line
	1700 1700 1750 1700
Wire Wire Line
	1700 1900 1750 1900
Wire Wire Line
	1000 1900 1200 1900
Wire Wire Line
	1000 1800 1200 1800
Wire Wire Line
	1200 1700 1000 1700
Wire Wire Line
	1700 1800 1800 1800
$Comp
L VCC #PWR05
U 1 1 5630B763
P 1750 1700
F 0 "#PWR05" H 1750 1550 50  0001 C CNN
F 1 "VCC" H 1750 1850 50  0000 C CNN
F 2 "" H 1750 1700 60  0000 C CNN
F 3 "" H 1750 1700 60  0000 C CNN
	1    1750 1700
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 5630B7C8
P 1750 1900
F 0 "#PWR06" H 1750 1650 50  0001 C CNN
F 1 "GND" H 1750 1750 50  0000 C CNN
F 2 "" H 1750 1900 60  0000 C CNN
F 3 "" H 1750 1900 60  0000 C CNN
	1    1750 1900
	1    0    0    -1  
$EndComp
Text Label 1000 1900 0    60   ~ 0
RST
Text Label 1000 1700 0    60   ~ 0
B6
Text Label 1000 1800 0    60   ~ 0
B7
Text Label 1800 1800 0    60   ~ 0
B5
Text Label 2900 1450 0    60   ~ 0
RST
$Comp
L SWITCH_INV SW1
U 1 1 5630C009
P 2050 800
F 0 "SW1" H 1850 950 50  0000 C CNN
F 1 "SWITCH_INV" H 1900 650 50  0000 C CNN
F 2 "terminal:switch_angle" H 2050 800 60  0001 C CNN
F 3 "" H 2050 800 60  0000 C CNN
	1    2050 800 
	1    0    0    -1  
$EndComp
Wire Wire Line
	1350 800  1550 800 
Wire Wire Line
	1350 900  1450 900 
Wire Wire Line
	1450 900  1450 1050
$Comp
L PWR_FLAG #FLG07
U 1 1 5630D17E
P 3600 1000
F 0 "#FLG07" H 3600 1095 50  0001 C CNN
F 1 "PWR_FLAG" H 3600 1180 50  0000 C CNN
F 2 "" H 3600 1000 60  0000 C CNN
F 3 "" H 3600 1000 60  0000 C CNN
	1    3600 1000
	1    0    0    -1  
$EndComp
Wire Wire Line
	3600 1000 3600 1100
Connection ~ 3600 1100
NoConn ~ 5300 2950
Wire Wire Line
	2550 900  2700 900 
NoConn ~ 2550 700 
$EndSCHEMATC
