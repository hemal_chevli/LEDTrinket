/* -----------------------------------------------------------------------
 * Title:    8x8 LED dot matrix animations
 * Author:   Alexander Weber alex@tinkerlog.com
 * Date:     21.12.2008
 * Hardware: ATtiny2313V
 * Software: AVRMacPack
 * 
 Modified by: Hemal Chevli hemal@black-electronics.com
 Software: WinAVR-20100110
 */

#include <inttypes.h>
#include <stdlib.h>
#include <string.h>
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/pgmspace.h>

#define MAX_ANIMATIONS 3

const uint8_t PROGMEM SpaceInvader1[8] = {
  0x24,    // __X__X__
  0x7E,    // _XXXXXX_
  0xDB,    // XX_XX_XX
  0xFF,    // XXXXXXXX
  0xA5,    // X_X__X_X
  0x99,    // X__XX__X
  0x81,    // X______X
  0xC3     // XX____XX
};

const uint8_t PROGMEM SpaceInvader2[8] = {
  0x24,    // __X__X__
  0x18,    // ___XX___
  0x7E,    // X_XXXX_X
  0xDB,    // XX_XX_XX
  0xFF,    // XXXXXXXX
  0xDB,    // X_XXXX_X
  0x99,    // X__XX__X
  0xC3     // XX____XX
};
const uint8_t PROGMEM SpaceInvader3[] = {
  0x18,    // ___XX___
  0x3C,    // __XXXX__
  0x7E,    // _XXXXXX_
  0xDB,    // X_XXXX_X
  0xFF,    // XXXXXXXX
  0x24,    // __X__X__
  0x5A,    // _X_XX_X_
  0xA5     // X_X__X_X
  };

const uint8_t PROGMEM SpaceInvader4[8] = {
  0x18,    // ___XX___ 
  0x3C,    // __XXXX__
  0x7E,    // _XXXXXX_
  0xDB,    // X_XXXX_X
  0xFF,    // XXXXXXXX
  0x24,    // __X__X__
  0x42,    // _X____X_
  0x24     // __X__X__
};

const uint8_t PROGMEM SpaceInvader5[8] = {
  0b00000000, // First frame for alien #2
  0b00111100,
  0b01111110,
  0b11011011,
  0b11011011,
  0b01111110,
  0b00100100,
  0b11000011,
};
 const uint8_t PROGMEM SpaceInvader6[8] = {
  0b00111100, // Second frame for alien #2
  0b01111110,
  0b11011011,
  0b11011011,
  0b01111110,
  0b00100100,
  0b00100100,
  0b00100100,
};
const uint8_t PROGMEM SpaceInvader7[8] = {
  0b00100100, // First frame for alien #3
  0b00100100,
  0b01111110,
  0b11011011,
  0b11111111,
  0b11111111,
  0b10100101,
  0b00100100,
};
 const uint8_t PROGMEM SpaceInvader8[8] = {
  0b00100100, // Second frame for alien #3
  0b10100101,
  0b11111111,
  0b11011011,
  0b11111111,
  0b01111110,
  0b00100100,
  0b01000010,
};
const uint8_t PROGMEM heart_small[8] = {
  0x00,    // ________ 
  0x00,    // ________
  0x14,    // ___X_X__
  0x3E,    // __XXXXX_
  0x3E,    // __XXXXX_
  0x1C,    // ___XXX__
  0x08,    // ____X___
  0x00     // ________
};

const uint8_t PROGMEM heart_big[8] = {
  0x00,    // ________ 
  0x66,    // _XX__XX_
  0xFF,    // XXXXXXXX
  0xFF,    // XXXXXXXX
  0xFF,    // XXXXXXXX
  0x7E,    // _XXXXXX_
  0x3C,    // __XXXX__
  0x18     // ___XX___
};
const uint8_t PROGMEM face1[8] = {
  0b11111111, // 1 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b10000001,
  0b11000011,
  0b11100111,
  0b11111111,
};
const uint8_t PROGMEM face2[8] = {
  0b11111111, // 2 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b10111101,
  0b10111101,
  0b11000011,
  0b11111111,
 };
 const uint8_t PROGMEM face3[8] = { 
  0b11111111, // 3 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b10000001,
  0b11000011,
  0b11100111,
  0b11111111,
  };
  const uint8_t PROGMEM face4[8] = {
  0b11111111, // 4 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b10111101,
  0b10111101,
  0b11000011,
  0b11111111,
  };
  const uint8_t PROGMEM face5[8] = {
  0b11111111, // 5 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b11111111,
  0b11111111,
  0b10000001,
  0b11111111,
  };
  const uint8_t PROGMEM face6[8] = {
  0b11111111, // 6 frame
  0b10011001,
  0b10011001,
  0b11111111,
  0b11100111,
  0b11011011,
  0b11100111,
  0b11111111,
};
  const uint8_t PROGMEM face7[8] = {
  0b11111111, // 7 frame
  0b10111101,
  0b00011000,
  0b10111101,
  0b11100111,
  0b11011011,
  0b11100111,
  0b11111111,
  };
  const uint8_t PROGMEM face8[8] = {
  0b11111111, // 8 frame
  0b11111111,
  0b00011000,
  0b11111111,
  0b11100111,
  0b11011011,
  0b11100111,
  0b11111111,
  };

const uint8_t PROGMEM whaa1[8] = {
  0b00111100, 
  0b01000010,
  0b10000001,
  0b10100101,
  0b10000001,
  0b10011001,
  0b10000001,
  0b11111111,
};

const uint8_t PROGMEM whaa2[8] = {
  0b00111100, 
  0b01000010,
  0b10100101,
  0b10000001,
  0b10000001,
  0b10011001,
  0b10000001,
  0b11111111,
};

const uint8_t PROGMEM smiley1[8] = {
  0b00111100,
  0b01000010,
  0b10101001,
  0b10101001,
  0b10000101,
  0b10111001,
  0b01000010,
  0b00111100,
};

const uint8_t PROGMEM smiley2[8] = {
  0b00111100,
  0b01000010,
  0b10000001,
  0b10101001,
  0b10000101,
  0b10111001,
  0b01000010,
  0b00111100,
};

const uint8_t PROGMEM sqsmiley[8] = {
  0b00111100,
  0b01000010,
  0b10101001,
  0b10101001,
  0b10000101,
  0b10111001,
  0b01000010,
  0b00111100,
};

const uint8_t PROGMEM rabbit1[8] = {
  0b01100110,
  0b01100110,
  0b01111110,
  0b11111110,
  0b10000001,
  0b10100101,
  0b10011001,
  0b01111110,  
};
const uint8_t PROGMEM rabbit2[8] = {
  0b10100101,
  0b01000010,
  0b01111110,
  0b10000001,
  0b10100101,
  0b10000001,
  0b10011001,
  0b01111110,
};
const uint8_t PROGMEM headbang1[8] = {
  0b10011001,
  0b10111101,
  0b01011010,
  0b01111110,
  0b01000010,
  0b00111100,
  0b11011011,
  0b10000001,
};

const uint8_t PROGMEM headband2[8] = {
  0b01011010,
  0b10111101,
  0b01111110,
  0b01011010,
  0b01111110,
  0b00100100,
  0b11011011,
  0b10000001,
};

const uint8_t PROGMEM stickman1[8] = {
  0b00010001,
  0b11111111,
  0b10011000,
  0b00011000,
  0b00011000,
  0b00100100,
  0b01000010,
  0b11000010, 
};

const uint8_t PROGMEM stickman2[8] = {
  0b10001000,
  0b11111111,
  0b00011001,
  0b00011000,
  0b00011000,
  0b00101000,
  0b11001000,
  0b10001100,
};

const uint8_t PROGMEM circle1[8] = {
  0b00111100,
  0b01000010,
  0b10100101,
  0b10011001,
  0b10011001,
  0b10100101,
  0b01000010,
  0b00111100,
};

const uint8_t PROGMEM circle2[8] = {
  0b00111100,
  0b01010010,
  0b10010001,
  0b10011111,
  0b11111001,
  0b10001001,
  0b01001010,
  0b00111100,
};

const uint8_t PROGMEM sq1[8] = {
  0b00000000,
  0b00000000,
  0b00000000,
  0b00011000,
  0b00011000,
  0b00000000,
  0b00000000,
  0b00000000,
};
const uint8_t PROGMEM sq2[8] = {
  0b00000000,
  0b00000000,
  0b00111100,
  0b00100100,
  0b00100100,
  0b00111100,
  0b00000000,
  0b00000000,
};
const uint8_t PROGMEM sq3[8] = {
  0b00000000,
  0b01111110,
  0b01000010,
  0b01000010,
  0b01000010,
  0b01000010,
  0b01111110,
  0b00000000,
};
const uint8_t PROGMEM sq4[8] = {
  0b11111111,
  0b10000001,
  0b10000001,
  0b10000001,
  0b10000001,
  0b10000001,
  0b10000001,
  0b11111111,
};


const uint8_t* const spriteArray[] PROGMEM = {
SpaceInvader1,
SpaceInvader2,
SpaceInvader3,
SpaceInvader4,
SpaceInvader5,
SpaceInvader6,
SpaceInvader7,
SpaceInvader8,
heart_small,
heart_big,
face1,
face2,
face3,
face4,
face5,
face6,
face7,
face8,
whaa1,
whaa2,
smiley1,
smiley2,
rabbit1,
rabbit2,
headbang1,
headband2,
stickman1,
stickman2,
circle1,
circle2,
sq1,
sq2,
sq3,
sq4,
};

uint8_t mode_ee EEMEM = 1;                      // stores the mode in eeprom
static uint8_t screen_mem[8];			// screen memory
static uint8_t active_row;			// active row
static uint8_t buffer[60];                      // stores the active message or sprite
static volatile uint16_t counter = 0;           // used for delay function
static volatile uint8_t mode = 0;     //used for button interrupt
volatile uint16_t rising, falling;
volatile uint16_t counts = 5;
//volatile uint16_t us_per_count;
volatile uint8_t cycle = 0;


// prototypes
void delay_ms(uint16_t delay);
void copy_to_display(int8_t x, int8_t y, uint8_t sprite[]);
void display_active_row(void);
void copy_to_buffer(const uint8_t sprite[8]);

/*
 * ISR TIMER0_OVF_vect
 * Handles overflow interrupts of timer 0.
 *
 * 4MHz
 * ----
 * Prescaler 8 ==> 1953.1 Hz
 * Complete display = 244 Hz
 *
 */
ISR(TIMER0_OVF_vect) {	
  display_active_row();
 // counter++;
}

/*
 * copy_to_display
 * Copies sprite data to the screen memory at the given position. 
 */
void copy_to_display(int8_t x, int8_t y, uint8_t sprite[8]) {
  int8_t i, t;
  uint8_t row;
  for (i = 0; i < 8; i++) {
    t = i-y;
    row = ((t >= 0) && (t < 8)) ? sprite[t] : 0x00;
    row = (x >= 0) ? (row >> x) : (row << -x);
    screen_mem[i] = row;
  }
}

/*
 * Use this method, if you have a common cathode matrix.
 */

void display_active_row(void) {

  uint8_t row;

  // shut down all rows and columns
  //PORTB = 0x34; 
  //PORTD = 0x1B; 
  //PORTA = 0x01;

  PORTA = 0x03;
  PORTB = 0xD6;
  PORTD = 0x08;
  // next row
  active_row = (active_row+1) % 8;
  row = screen_mem[active_row];

  // output all columns, switch leds on.
  // column 1
  if ((row & 0x80) == 0x80) {
    PORTB &= ~(1 << PB4);    
  }
  // column 2
  if ((row & 0x40) == 0x40) {
    PORTA &= ~(1 << PA1);    
  }
  // column 3
  if ((row & 0x20) == 0x20) {
    PORTA &= ~(1 << PA0);    
  }
  // column 4
  if ((row & 0x10) == 0x10) {
    PORTB &= ~(1 << PB1);    
  }
  // column 5
  if ((row & 0x08) == 0x08) {
    PORTD &= ~(1 << PD3);    
  }
  // column 6
  if ((row & 0x04) == 0x04) {
    PORTB &= ~(1 << PB2);    
  }
  // column 7
  if ((row & 0x02) == 0x02) {
    PORTB &= ~(1 << PB6);    
  }
  // column 8
  if ((row & 0x01) == 0x01) {
    PORTB &= ~(1 << PB7);    
  }

  // activate row
  switch (active_row) {
  case 0:
    PORTB |= (1 << PB0);
    break;
  case 1:
    PORTB |= (1 << PB5);
    break;
  case 2:
    PORTD |= (1 << PD5);
    break;
  case 3:
    PORTB |= (1 << PB3);
    break;
  case 4:
    PORTD |= (1 << PD0);
    break;
  case 5:
    PORTD |= (1 << PD4);
    break;
  case 6:
    PORTD |= (1 << PD1);
    break;
  case 7:
    PORTD |= (1 << PD2);
    break;
  }

}

/*
 * copy_to_buffer
 * Copies the given sprite from PROGMEM to RAM.
 */
void copy_to_buffer(const uint8_t sprite[8]) {
//void copy_to_buffer(uint8_t i) {
  memcpy_P(buffer, sprite, 8);
  //memcpy_P(cmdBuffer, (char*)pgm_read_word(&(spriteArray[i])));
   //strcpy_P(cmdBuffer, (char*)pgm_read_word(&(POSTMethod[3])));
}

void animate(const uint8_t frame1[8],const uint8_t frame2[8]){
  copy_to_buffer(frame1);  
  copy_to_display(0, 0, buffer);
  _delay_ms(750);
  copy_to_buffer(frame2);        
  copy_to_display(0, 0, buffer);
  _delay_ms(750);

}
//create ISR for button press
//create timer intrrupt to check button press
ISR(TIMER1_CAPT_vect){
  _delay_ms(150);  //debounce
  if (TCCR1B & (1<<ICES1)) 
    {
      TCCR1B &= ~(1<<ICES1);
      rising = ICR1;
      mode++; //remove this
    }
    else
    {
      TCCR1B |= (1<<ICES1);
      falling = ICR1;
      counts = (uint32_t)falling - (uint32_t)rising;
    }
}

int main(void) {

  uint8_t i = 0;
  // define outputs
  DDRA |= 0x03;  
  DDRB |= 0xFF;
  DDRD |= 0x3F;
  
  // shut down all rows and columns, enable column 1
  
  PORTA = 0x03;
  PORTB = 0xD6;
  PORTD = 0x08;

  // enable pull ups
  DDRD &= ~(1 << PD6);    // switch input
  //PORTD &= ~(1 << PD6);    // enable pull-up resistor

  // read last mode from eeprom
  // 0 mean cycle through all modes and messages
//  mode = eeprom_read_byte(&mode_ee);
//set reg for input capture on PD6
 // timer 0 setup, prescaler 8
  TCCR0B |= (1 << CS01);
  // enable timer 0 interrupt
  TIMSK |= (1 << TOIE0);  


  TIMSK |= (1<<ICIE1);  //TICIE1: Timer 1 Input Capture Interrupt Enable,
  TCCR1B |= (1<<ICNC1) | (1<<CS10) | (1<<CS11) | (1<<WGM12); //ICNC1: noise filter, CS10 and CS11: devide clock by 64, WGM12: Clear Timer on Compare (CTC) Mode
  TCCR1B &= ~(1<<ICES1); //falling edge
  OCR1A = 8750;
  
  cycle =0;
  counts=0;
  mode = 0;
  sei();

  while (1) {
  //long press 
    //cycle or stop cycle animations
  //short press 
    //change mode  
    if(mode >14){
      mode = 0;
    }
    animate((uint8_t*)pgm_read_word(&(spriteArray[mode*2])), (uint8_t*)pgm_read_word(&(spriteArray[(mode*2)+1])));
  }
  return 0;
}